# Rapport du projet Microservices

### Michel FLORANTIN - Raymond BALLO - Marwane GHARS

## Introduction

L'objectif de ce projet est de développer une application de location de logement basée sur l'architecture des microservices. Dans ce rapport, nous présentons notre conception pour cette application en détail ainsi que la description des technologies adoptés. La conception est le résultat final de notre application.

## Architecture logicielle

Nous avons proposé une architecture logicielle qui se compose de cinq services principaux :

1.  Service de gestion des utilisateurs
2.  Service de recherche de logement
3.  Service de gestion de la location
4.  Service de gestion des paiements
5.  Service de notification

Nous allons maintenant expliquer chaque service en détail :

### Service de gestion des utilisateurs

Ce service permet l'enregistrement et l'authentification des utilisateurs (locataires et propriétaires) et la gestion des informations de profil. Les utilisateurs peuvent s'inscrire en tant que locataires ou propriétaires et créer un profil en fournissant leur nom, prénom et adresse e-mail. Le service permet également aux utilisateurs de mettre à jour leur profil. Ce service est accessible via une interface utilisateur personnalisable pour les locataires et les propriétaires. Il est également accessible via une interface d'authentification pour permettre l'authentification des utilisateurs. Ce service utilise une API REST pour fournir des fonctionnalités de base telles que la création d'un nouvel utilisateur, la récupération des informations de profil et la mise à jour des informations de profil.

### Service de recherche de logement

Ce service permet la recherche de logements disponibles par type, emplacement et budget. Les utilisateurs peuvent rechercher des logements disponibles en fonction de leur type, de leur emplacement et de leur budget. Ce service est accessible via une interface utilisateur personnalisable pour les locataires, les propriétaires et l'administrateur. Ce service utilise une API REST pour fournir des fonctionnalités de recherche de base telles que la récupération des informations sur les logements disponibles.

### Service de gestion de la location

Ce service permet la gestion des demandes de location et des offres de location. Il traite les demandes de réservation de logement et assure le suivi des processus de location. Les utilisateurs peuvent effectuer des demandes de réservation de logement et suivre l'état de leur demande. Ce service est accessible via une interface utilisateur personnalisable pour les locataires, les propriétaires et l'administrateur. Ce service utilise une API REST pour fournir des fonctionnalités de base telles que la création d'une demande de réservation, la récupération de l'état de la demande de réservation et la mise à jour de l'état de la demande de réservation.

### Service de gestion des paiements

Ce service permet la gestion des paiements entre locataires et propriétaires. Les utilisateurs peuvent effectuer des paiements pour réserver un logement. Ce service est accessible via une interface utilisateur personnalisable pour les locataires et les propriétaires. Ce service utilise une API REST pour fournir des fonctionnalités de base telles que la création d'un nouveau paiement et la récupération des informations sur les paiements effectués.

### Service de notification

Ce service permet l'envoi de notifications pour informer les utilisateurs des mises à jour importantes telles que les demandes de réservation de logement, les confirmations de paiement, les annulations de location, etc.

## Description des technologies adoptées

Notre application de location de logement basée sur l'architecture des microservices est développée en utilisant les technologies suivantes :

1. OpenFeign
2. SpringBoot
3. Spring Security
4. Docker
5. Oracle SQL Developer
6. Swagger2

Nous allons maintenant expliquer chaque technologie en détail et pourquoi les avoir choisi :

### OpenFeign

OpenFeign est une bibliothèque Java utilisée pour la création de clients REST. Dans notre architecture de microservices, les différents services ont besoin de communiquer entre eux pour échanger des informations. OpenFeign facilite cette communication en simplifiant la création de clients REST pour les appels entre les différents services. Cette bibliothèque permet également de configurer des requêtes HTTP personnalisées, de gérer les erreurs de manière centralisée, ainsi que de faciliter la mise en cache des résultats.

### Spring Boot

Spring Boot est un framework Java utilisé pour créer des applications web en utilisant l'architecture des microservices. Il fournit une solution tout-en-un pour le développement d'applications en fournissant une configuration par défaut pour de nombreuses fonctionnalités courantes. Dans notre projet de location de logement, nous avons utilisé Spring Boot pour gérer la configuration des différents services, ainsi que pour simplifier le processus de développement et de déploiement.

### Spring Security 

Spring Security est un module de sécurité de Spring qui permet de gérer l'authentification et les autorisations dans les applications web. Dans notre projet, nous avons utilisé Spring Security pour gérer l'authentification et les habilitations des utilisateurs. Il nous permet de mettre en place une sécurité robuste pour l'ensemble de l'application en gérant les différents aspects liés à l'authentification et à la sécurité, tels que l'encryption des mots de passe, la gestion des sessions et la gestion des rôles utilisateurs.

### Docker

Docker est une plateforme de virtualisation de conteneurs qui permet de créer, déployer et exécuter des applications de manière isolée. Dans notre projet, nous avons utilisé Docker pour gérer le déploiement des différents services de notre application. Docker permet de faciliter le processus de déploiement en créant des conteneurs pour chaque service, en gérant la communication entre les différents conteneurs et en garantissant que chaque service dispose de l'environnement d'exécution dont il a besoin pour fonctionner correctement.

### Oracle SQL Developer

Oracle SQL Developer est un outil de développement de bases de données relationnelle. Il fournit un large éventail de fonctionnalités pour la gestion et le développement des bases de données, y compris la création de tables, la modification de schémas, la création de vues et de procédures stockées, la génération de requêtes SQL, la gestion des utilisateurs et des privilèges.

### Swagger2

Swagger2 est une spécification et un ensemble d'outils open-source pour la conception, la construction, la documentation et la consommation d'API RESTful. Il permet de documenter l'ensemble des endpoints et des paramètres de requête de notre API REST, ainsi que de générer automatiquement une documentation interactive à partir de ces informations. Swagger2 facilite également la création de clients et de serveurs REST pour les développeurs en générant automatiquement du code source basé sur la spécification Swagger.

## Liens

Git :
https://gitlab.com/mflorantin/rent-a-house/-/tree/master?ref_type=heads

Sql :
https://gitlab.com/mflorantin/rent-a-house/-/blob/master/export_database_rent-a-house.sql

Swaggers (lancer le service) :
http://localhost:9898/swagger-ui/index.html
http://localhost:9899/swagger-ui/index.html

Images :
https://drive.google.com/drive/folders/1L5_y-5EuSYZzQDcnfvR6bnpX23adG8cX?usp=sharing

Collections Postman :
https://gitlab.com/mflorantin/rent-a-house/-/blob/master/Rent-a-House_Collection.postman_collection.json
https://gitlab.com/mflorantin/rent-a-house/-/blob/master/Rent-a-House_Logement_Collections.postman_collection.json
